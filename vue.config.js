module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  css: {
      extract: { ignoreOrder: true },
  },
  chainWebpack: config => {
   config.performance
     .maxEntrypointSize(1000000)
     .maxAssetSize(1000000)
 }
}
