export default {
  Kontakte:{
    defaultSearchField:'nachname',
    searchFields:['vorname','nachname','strasse','hausnummer','plz','ort','email'],
    listName: 'kontaktes',
    listTitle:'Kontakte',
    model:{
      id:false,
      vorname: {
        text: "Vorname",
        required: true,
        rules:"notEmptyRules",
        type: 'text'
      },
      nachname: {
        text: "Nachname",
        required: true,
        rules:"notEmptyRules",
        type: 'text'
      },
      strasse: "Straße",
      hausnummer: "Hausnummer",
      plz: "PLZ",
      ort: "Ort",
      land: "Land",
      email: "E-Mail",
      telefon: "Telefon",
      mobile: "Mobil",
      fax: "Fax",
      golfverein:{
        query: true,
        type:'select',
        listName:"golfvereines",
        text:"Golfverein",
        value:"id",
        short:"clubname",
        long:"clubname",
        model:{
          id: false,
          clubname:"Clubname"
        }
      }
    },
    key:'golfverein',
    mutationKey:'kontakte',
    typeKey: 'Kontakte'
  },
  Bestenliste:{
    defaultSearchField:'nachname',
    searchFields:['vorname','nachname','punkte','position','titel'],
    listName: 'bestenlistes',
    listTitle:'Bestenliste',
    typeKey:'Bestenliste',
    newRecordBtnTxt: 'Neuer Eintrag',
    mutationKey:'bestenliste',
    model:{
      id:false,
      /*anrede: {
          type: 'select',
          text: 'Anrede',
          options:[
            {
              v:'',
              d:'Anrede wählen'
            },
            {
              v:'Herr',
              d:'Herr'
            },
            {
              v:'Frau',
              d:'Frau'
            }
          ]
      },*/
      titel: "Titel",
      vorname: "Vorname",
      nachname: "Nachname",
      Jahr: "Jahr",
    /*  golfverein:{
        query:true,
        type:'select',
        listName:"golfvereines",
        text:"Golfverein",
        value:"id",
        short:"clubname",
        long:"clubname",
        model:{
          id: false,
          clubname:"Clubname"
        }
      },
      turnier: {
        type: 'select',
        query:true,
        listName:"turnieres",
        text:"Turnier",
        value:"id",
        short:"turnier_nr",
        long:"turnier_nr",
        model:{
          id: false,
          turnier_nr:{
            type:"number",
            text:"Turnier-Nr",
            required: true,
            rules:"numberRules",
          },
        }
      }*/
    }
  },
  Vereine:{
   defaultSearchField:'clubname',
    searchFields:['clubname','clubnummer','plz','ort','eingetreten_am','ausgetreten_am'],
    listName: 'golfvereines',
    listTitle:'Golfvereine',
    typeKey:'Golfvereine',
    mutationKey:'golfvereine',
    model:{
      id:false,
      clubnummer: {
        text: "Clubnummer",
        editable: true,
        required: true,
        rules:"numberRules",
        type: 'text'
      },
      clubname: "Clubname",
      strasse: {
        text:"Straße",
        visible:false
      },
      hausnummer: {
        text:"Hausnummer",
        visible:false
      },
      plz: "PLZ",
      ort: "Ort",
      telefon:{
        type:"tel",
        text: "Telefon"
      },
      "eingetreten_am":{
        text: "Eintrittsdatum",
        type: "date",
        colspan: 4
      },
      "ausgetreten_am":{
        text: "Austrittsdatum",
        type: "date",
        colspan: 4
      },
      "gemeinnuetzig":{
        text: "Gemmeinnützig",
        type: "checkbox",
        colspan: 4
      },
      lochzahl:{
        type:"select",
        query: true,
        listName:"lochzahls",
        value:"lochzahl",
        reference: false,
        short:"lochzahl",
        long:"lochzahl",
        model:{
          id: false,
          lochzahl:"Lochzahl"
        },
        colspan:4
      },

      "clubverwaltungssoftware":{
        text:"Clubverwaltungs-Software",
        type:"text",
        colspan:4,
        visible:false
      },
      "mitglied_landessportbund":{
        text: "Mitglied im Landessportbund",
        type: "checkbox",
        colspan: 4,
        visible:false
      },

     "kontoinhaber":{
        text:"Kontoinhaber",
        type:"text",
        visible:false
      },
     "bank":{
        text:"Bank",
        type:"text",
        visible:false
      },
     "iban":{
        text:"IBAN",
        type:"text",
        visible:false
      },
     "bic":{
        text:"BIC",
        type:"text",
        visible:false
      },
      "kommentar":{
        text:"Kommentar",
        editable: true,
        visible:false,
        type:"textarea",
        max:"500",
        colspan:12
      },


      "golfvereines":{
        text: "Uploads",
        type: "media",
        editable: ['caption'],
        newRcrdBtnTxt: 'Dateien hochladen',
        key:'id',
        mutationKey:'file',
        typeKey: 'File',
        createMutation:'multipleUpload',
        model:{
          id: false,
          uploads:{
            type:"media",
            model:{
              id: false,
              name:"Name",
              caption:"Beschreibung",
              url: "Vorschau",
              formats:false
            }
          }
        }
      },
      "kontaktes":{
        text:"Kontakte",
        type:"list",
        model:{
          id:false,
          vorname: {
            text: "Vorname",
            required: true,
            rules:"notEmptyRules",
            type: 'text'
          },
          nachname: {
            text: "Nachname",
            required: true,
            rules:"notEmptyRules",
            type: 'text'
          },
          strasse: "Straße",
          hausnummer: "Hausnummer",
          plz: "PLZ",
          ort: "Ort",
          land: "Land",
          email: "E-Mail",
          telefon: "Telefon",
          mobile: "Mobil",
          fax: "Fax"
        },
        key:'golfverein',
        mutationKey:'kontakte',
        typeKey: 'Kontakte'
      },
      "mannschaftens":{
        text:"Mannschaften",
        type:"list",
        model:{
          id:false,
          kennung: "Kennung",
          name: "Name"
        },
        key:'golfverein',
        mutationKey:'mannschaften',
        typeKey: 'Mannschaften'
      },
      "mitgliederzahls":{
        text:"Mitgliederzahlen",
        type:"list",
        model:{
          id:false,
          m_1_6: {
            type: "number",
            text: "M-1-6"
          },
          w_1_6: {
            type: "number",
            text: "W-1-6"
          },
          m_7_10: {
            type: "number",
            text: "M-1-6"
          },
          w_7_10: {
            type: "number",
            text: "M-1-6"
          },
          w_11_13: {
            type: "number",
            text: "w_11_13"
          },
          m_14_16: {
            type: "number",
            text: "m_14_16"
          },
          m_11_13: {
            type: "number",
            text: "m_11_13"
          },
          w_14_16: {
            type: "number",
            text: "w_14_16"
          },
          m_17_18: {
            type: "number"
          },
          w_17_18: {
            type: "number"
          },
          m_19_55: {
            type: "number"
          },
          w_19_55: {
            type: "number"
          },
          m_56_x: {
            type: "number"
          },
          w_56_x: {
            type: "number"
          },
          stichtag: {
            type: "date",
            text: "Stichtag"
          }
        },
        key:'golfverein',
        mutationKey:'mitgliederzahl',
        typeKey: 'Mitgliederzahl'
      },
      "turnieres":{
        text:"Turniere",
        type:"list",
        mutationKey:'turniere',
        typeKey: 'Turniere',
        key:'austraeger',
        model:{
          id:false,
          turnier_nr:{
            type:"number",
            text:"Turnier-Nr",
            required: true,
            rules:"numberRules",
          },
          starttermin: {
            type:"datetime",
            text:"Turnierstart"
          },
          endtermin: {
            type: "datetime",
            text: "Turnierende"
          },
          austragungsort: "Austragungsort",
          turnierarten: {
            type:"select",
            required: true,
            rules:"notEmptyRules",
            listName:"turnierartens",
            text:"Turnierart",
            value:"id",
            short:"name",
            long:"name",
            model:{
              id: false,
              kennung: "Kennung",
              name: "Name"
            }
          },
          turnier_altersklasse: {
            type:"select",
            required: true,
            rules:"notEmptyRules",
            listName:"turnierAltersklasses",
            text:"Altersklasse",
            value:"id",
            short:"altersklasse_kurz",
            long:"altersklasse",
            model:{
              id: false,
              altersklasse: "Altersklasse",
              altersklasse_kurz: "altersklasse_kurz"
            }
          },
          turnier_geschlecht: {
            type:"select",
            text:"Geschlecht",
            required: true,
            rules:"notEmptyRules",
            listName:"tunierGeschlechts",
            value:"id",
            short:"geschlecht_kurz",
            long:"geschlecht",
            model:{
              id: false,
              geschlecht: "Geschlecht",
              geschlecht_kurz: "Kurzform Geschlecht"
            }
          }
        }
      },
    }
  }
};
