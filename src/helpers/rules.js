export default {
  notEmptyRules: [
    v => !!v || 'Dies ist ein Pflichtfeld.'
  ],
  emailRules: [
    v => !!v || 'Bitte geben Sie eine E-Mail-Adresse ein.',
    v => /.+@.+/.test(v) || 'Die eingegebene E-Mail-Adresse scheint nicht gültig zu sein.',
  ],
  numberRules:[
    v => !!v || 'Bitte geben Sie eine Nummer ein.',
    v => /^[0-9]+$/.test(v) || 'Bitte nur Zahlen eingeben.',
  ],
}
