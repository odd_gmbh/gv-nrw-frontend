import axios from 'axios';

export const HTTP = axios.create({
  baseURL: process.env.VUE_APP_STRAPI_API_URL || "http://localhost:1337",
  headers: {
    authorization: localStorage.getItem('token') || ''
  }
})
